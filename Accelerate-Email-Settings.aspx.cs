﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Email_Settings : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    clsEmailSettings clsEmailsettings;

    #region #EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all sesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        try
        {
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        }
        catch (Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }

        //try
        //{
        //    clsEmailsettings = new clsEmailSettings(clsAccountUsers.iAccountUserID);
        //    rdbMissions.Checked = clsEmailsettings.bEmailMissions;
        //    rdbForums.Checked = clsEmailsettings.bEmailForumMessages;
        //    rdbPhase.Checked = clsEmailsettings.bEmailPhaseReminders;
        //}
        //catch
        //{

        //}
    }

    #endregion

    #region #POPULATE DATA

    protected void popProfile(int iCurrentPoints)
    {
        string strFullName = clsAccountUsers.strFirstName + " " + clsAccountUsers.strSurname.Substring(0, 1);
        string strFullPath = "";
        string strFileName = "";

        //### Populates images
        if (!string.IsNullOrEmpty(clsAccountUsers.strMasterImage))
        {
            strFullPath = clsAccountUsers.strPathToImages;
            if (!string.IsNullOrEmpty(clsAccountUsers.strMasterImage))
            {
                strFileName = clsAccountUsers.strMasterImage;
                strFullPath = "AccountUsers/" + clsAccountUsers.strPathToImages + "/" + Path.GetFileNameWithoutExtension(strFileName) + Path.GetExtension(strFileName);
            }
        }
        else
            strFullPath = "images/no-image.png";

        StringBuilder strbAccountUser = new StringBuilder();

        strbAccountUser.AppendLine("<div class='homeHeaderContainer'>");
        strbAccountUser.AppendLine("<div class='headerImgContainer'><img src='" + strFullPath + "' width='100px' height='100px'/></div>");
        strbAccountUser.AppendLine("<div class='headerContentContainer'>");
        strbAccountUser.AppendLine("<h1 class='ProfileHead'>" + strFullName + "</h1>");
        strbAccountUser.AppendLine("<div class='progressContainerProfile'> ");
        strbAccountUser.AppendLine("<span class='white'>Current rank and points</span>");
        int iCurrentRank = iRankID(iCurrentPoints);
        clsRanks clsCurrentRank = new clsRanks(iCurrentRank);
        clsRanks clsNextRank = new clsRanks();

        if (iRankCount() > iCurrentRank)
        {
            clsNextRank = new clsRanks(iCurrentRank + 1);
        }

        strbAccountUser.AppendLine("<p class='green'>" + clsCurrentRank.strTitle + ": " + iCurrentPoints + "</p>");

        //###   Bootstrap Progress Bar
        strbAccountUser.AppendLine("<div class='progress progress-striped active'>");
        strbAccountUser.AppendLine("<div class='progress-bar' role='progressbar' aria-valuenow='45' aria-valuemin='0' aria-valuemax='100' style='width: " + iCurrentPercentage(iCurrentPoints, clsCurrentRank.iPointLevel) / 2 + "%'>");
        strbAccountUser.AppendLine("<span class='sr-only'>" + iCurrentPercentage(iCurrentPoints, clsCurrentRank.iPointLevel) / 2 + "% Complete</span>");
        strbAccountUser.AppendLine("</div>");
        strbAccountUser.AppendLine("</div>");

        //###   Custom Progress Bar
        //strbAccountUser.AppendLine("<div class='headerProgressBarEmpty'>");
        //strbAccountUser.AppendLine("<div class='percentageHeaderProgressBar' style='width:" + iCurrentPercentage(iCurrentPoints, clsCurrentRank.iPointLevel) + "px'>&nbsp;</div>");
        //strbAccountUser.AppendLine("</div>");
        strbAccountUser.AppendLine("</div>");
        strbAccountUser.AppendLine("<div class='progressContainerProfile'>");
        strbAccountUser.AppendLine("<span class='white'>Next rank and points</span>");

        if (iRankCount() > iCurrentRank)
        {
            strbAccountUser.AppendLine("<p class='orange'>" + clsNextRank.strTitle + ": " + (clsNextRank.iPointLevel - clsCurrentRank.iPointLevel) + " MORE</p>");
        }
        else
        {
            strbAccountUser.AppendLine("<p class='orange'>Well Done! You're at the top!</p>");
        }

        strbAccountUser.AppendLine("</div>");
        strbAccountUser.AppendLine("</div>");
        strbAccountUser.AppendLine("<br class='clr'/>");
        strbAccountUser.AppendLine("</div>");

        litProfile.Text = strbAccountUser.ToString();
    }

    protected void popGroupMembers(int iGroupID)
    {
        DataTable dtAccountMemberMain = clsAccountUsers.GetAccountUsersList("iGroupID=" + iGroupID, "strFirstName ASC");
        dtAccountMemberMain.Columns.Add("FullPathForImage");

        string strFirstName;
        int iAccountMemberID;
        int iCount = 0;
        string strAccountMemberMain = "";
        string strPathToImage = "";
        StringBuilder sbAccountMember = new StringBuilder();

        foreach (DataRow dtrAccountMember in dtAccountMemberMain.Rows)
        {
            ++iCount;

            iAccountMemberID = Convert.ToInt32(dtrAccountMember["iAccountUserID"].ToString());
            strFirstName = dtrAccountMember["strFirstName"].ToString();
            //string strDescription = dtrAccountMember["strDescription"].ToString();

            if ((dtrAccountMember["strMasterImage"].ToString() == "") || (dtrAccountMember["strMasterImage"] == null))
            {
                sbAccountMember.AppendLine("<a><img src='images/no-image.png'  title='" + strFirstName + "' alt='" + strFirstName + "' width='62px' height='62px' /></a>");
            }
            else
            {
                strPathToImage = "AccountUsers/" + dtrAccountMember["strPathToImages"] + "/" + Path.GetFileNameWithoutExtension(dtrAccountMember["strMasterImage"].ToString()) + "" + Path.GetExtension(dtrAccountMember["strMasterImage"].ToString());
                sbAccountMember.AppendLine("<a><img src='" + strPathToImage + "'  title='" + strFirstName + "' alt='" + strFirstName + "' width='62px' height='62px' /></a>");
            }
        }

        //## Populates members in subgroup
        DataTable dtGroupList = clsGroups.GetGroupsList("iGroupID=" + iGroupID, "");

        foreach (DataRow dtrGroup in dtGroupList.Rows)
        {
            int iSubGroupID = Convert.ToInt32(dtrGroup["iLevelID"]);

            dtAccountMemberMain = clsAccountUsers.GetAccountUsersList("iGroupID=" + iSubGroupID, "strFirstName ASC");

            foreach (DataRow dtrAccountMember in dtAccountMemberMain.Rows)
            {
                ++iCount;

                iAccountMemberID = Convert.ToInt32(dtrAccountMember["iAccountUserID"].ToString());
                strFirstName = dtrAccountMember["strFirstName"].ToString();
                //string strDescription = dtrAccountMember["strDescription"].ToString();

                if ((dtrAccountMember["strMasterImage"].ToString() == "") || (dtrAccountMember["strMasterImage"] == null))
                {
                    sbAccountMember.AppendLine("<a><img src='images/no-image.png'  title='" + strFirstName + "' alt='" + strFirstName + "' width='62px' height='62px' /></a>");
                }
                else
                {
                    strPathToImage = "AccountUsers/" + dtrAccountMember["strPathToImages"] + "/" + Path.GetFileNameWithoutExtension(dtrAccountMember["strMasterImage"].ToString()) + "" + Path.GetExtension(dtrAccountMember["strMasterImage"].ToString());
                    sbAccountMember.AppendLine("<a><img src='" + strPathToImage + "'  title='" + strFirstName + "' alt='" + strFirstName + "' width='62px' height='62px' /></a>");
                }
            }
        }
        strAccountMemberMain = sbAccountMember.ToString();
        litGroupMemberMain.Text = strAccountMemberMain.ToString();
    }

    protected DataTable getAllPhasesForCurrentUserGroup(int iCurrentGroup)
    {
        DataTable dtPhaseList = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iCurrentGroup, ""); //+ " AND iAccountUserID=" + clsAccountUsers.iAccountUserID

        //int iTotalPhases = dtPhaseList.Rows.Count;

        return dtPhaseList;
    }

    protected DataTable getAchievementsToDisplayDT(string strAchievementsList)
    {
        DataTable dtAchievement = clsAchievements.GetAchievementsList();
        dtAchievement.Columns.Add("FullPathForImage");

        DataTable dtAchievementsToDisplay = new DataTable();
        dtAchievementsToDisplay = dtAchievement.Clone();
        dtAchievementsToDisplay.Clear();

        string[] AchievementIDs = strAchievementsList.Split(',');

        foreach (string ID in AchievementIDs)
        {
            foreach (DataRow dtrAvailableAchievements in dtAchievement.Rows)
            {
                if (Convert.ToInt32(ID) == Convert.ToInt32(dtrAvailableAchievements["iAchievementID"].ToString()))
                    dtAchievementsToDisplay.Rows.Add(dtrAvailableAchievements.ItemArray);
            }
        }

        return dtAchievementsToDisplay;
    }

    protected string popCompletedAchievements(int iAccelerateUserID)
    {
        DataTable dtUserAchievement = clsUserAchievements.GetUserAchievementsList("iAccountUserID=" + iAccelerateUserID, "");

        string strListOfIDs = "";
        int iAchievementID;
        int iCount = 0;

        foreach (DataRow dtrAccountMember in dtUserAchievement.Rows)
        {
            ++iCount;

            iAchievementID = Convert.ToInt32(dtrAccountMember["iAchievementID"].ToString());
            strListOfIDs += iAchievementID;

            //clsAchievements clsAchievements = new clsAchievements(iAchievementID);

            if (dtUserAchievement.Rows.Count != iCount)
                strListOfIDs += ",";
        }

        return strListOfIDs;
    }

    private int canWeUnlockNextPhase() //###return 1 2 or 3 depending what should be available
    {
        int iGroupID = clsAccountUsers.iGroupID;
        clsGroups clsGroups = new clsGroups(clsAccountUsers.iGroupID);
        DataTable dtGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iGroupID, "");
        int iPhaseID = 1;
        int iPhaseCount = 1;

        foreach (DataRow dtrGroupPhasesList in dtGroupPhases.Rows)
        {
            iPhaseID = Convert.ToInt32(dtrGroupPhasesList["iPhaseID"]);
            DataTable dtPhaseMissions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iPhaseID, "");

            bool bIsThisPhaseComplete = true;

            //break;
            int iMissionID = 0;
            foreach (DataRow dtrPhaseMissionsList in dtPhaseMissions.Rows)
            {
                iMissionID = Convert.ToInt32(dtrPhaseMissionsList["iMissionID"]);

                clsMissions clsMissions = new clsMissions(iMissionID);
                int iMissionTypeID = clsMissions.iMissionTypeID;

                if (iMissionTypeID == 1)
                {
                    if (!bIsTextMissionComplete(iMissionID))
                        bIsThisPhaseComplete = false;
                }
                else if (iMissionTypeID == 2)
                {
                    if (!bIsLeadershipMissionComplete(iMissionID))
                        bIsThisPhaseComplete = false;
                }
            }

            if (bIsThisPhaseComplete)
                ++iPhaseCount;
        }

        return iPhaseCount;
    }

    protected int iRankID(int iCurrentPoints)
    {
        DataTable dtRank = clsRanks.GetRanksList("", "");

        int iRankID = 1;
        int iPointLevel;

        int iCount = 0;

        foreach (DataRow dtrRank in dtRank.Rows)
        {
            ++iCount;

            iRankID = Convert.ToInt32(dtrRank["iRankID"].ToString());
            iPointLevel = Convert.ToInt32(dtrRank["iPointLevel"].ToString());

            if (iCurrentPoints < iPointLevel)
            {
                break;
            }
        }

        return iRankID;
    }

    protected int iRankCount()
    {
        DataTable dtRank = clsRanks.GetRanksList();

        int iRowCount = dtRank.Rows.Count;

        return iRowCount;
    }

    protected int iCurrentPercentage(int iCurrentPoints, int iTotalPoints)
    {
        double dblCurrentPercentage = Convert.ToDouble(iCurrentPoints) / Convert.ToDouble(iTotalPoints);

        int iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);

        return iCurrentPercentage;
    }

    protected bool bIsTextMissionComplete(int iCurrentMissionID)
    {
        bool bIsTextMissionComplete = false;

        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        if (iCurrentPercentage == 200)
            bIsTextMissionComplete = true;

        return bIsTextMissionComplete;
    }

    protected bool bIsLeadershipMissionComplete(int iCurrentMissionID)
    {
        bool bIsLeadershipMissionComplete = false;

        DataTable dtMissionQuestions = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsTeamShipAnswers.GetTeamShipAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedby=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        if (iCurrentPercentage == 200)
            bIsLeadershipMissionComplete = true;

        return bIsLeadershipMissionComplete;
    }

    protected int iCurrentPercentage(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        return iCurrentPercentage;
    }

    protected int iCurrentPercentageTeamship(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsTeamShipAnswers.GetTeamShipAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedby=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        return iCurrentPercentage;
    }

    #endregion

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            clsEmailsettings.Update(); //method to insert/update after values have been inserted.
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
    }
}