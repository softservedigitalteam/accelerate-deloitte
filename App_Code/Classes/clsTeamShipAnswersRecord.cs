
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsTeamShipAnswers
/// </summary>
public class clsTeamShipAnswers
{
    #region MEMBER VARIABLES

        private int m_iTeamShipAnswerID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iTeamShipID;
        private int m_iMissionID;
        private String m_strNameAnswer;
        private String m_strTitleAnswer;
        private bool m_bIsDisplayed;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iTeamShipAnswerID
        {
            get
            {
                return m_iTeamShipAnswerID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iTeamShipID
        {
            get
            {
                return m_iTeamShipID;
            }
            set
            {
                m_iTeamShipID = value;
            }
        }

        public int iMissionID
        {
            get
            {
                return m_iMissionID;
            }
            set
            {
                m_iMissionID = value;
            }
        }

        public String strNameAnswer
        {
            get
            {
                return m_strNameAnswer;
            }
            set
            {
                m_strNameAnswer = value;
            }
        }

        public String strTitleAnswer
        {
            get
            {
                return m_strTitleAnswer;
            }
            set
            {
                m_strTitleAnswer = value;
            }
        }

        public bool bIsDisplayed
        {
            get
            {
                return m_bIsDisplayed;
            }
            set
            {
                m_bIsDisplayed = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsTeamShipAnswers()
    {
        m_iTeamShipAnswerID = 0;
    }

    public clsTeamShipAnswers(int iTeamShipAnswerID)
    {
        m_iTeamShipAnswerID = iTeamShipAnswerID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iTeamShipAnswerID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iTeamShipID", m_iTeamShipID),
                        new SqlParameter("@iMissionID", m_iMissionID),
                        new SqlParameter("@strNameAnswer", m_strNameAnswer),
                        new SqlParameter("@strTitleAnswer", m_strTitleAnswer),
                        new SqlParameter("@bIsDisplayed", m_bIsDisplayed)                  
                  };

                  //### Add
                  m_iTeamShipAnswerID = (int)clsDataAccess.ExecuteScalar("spTeamShipAnswersInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iTeamShipAnswerID", m_iTeamShipAnswerID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iTeamShipID", m_iTeamShipID),
                         new SqlParameter("@iMissionID", m_iMissionID),
                         new SqlParameter("@strNameAnswer", m_strNameAnswer),
                         new SqlParameter("@strTitleAnswer", m_strTitleAnswer),
                         new SqlParameter("@bIsDisplayed", m_bIsDisplayed)
                    };
                    //### Update
                    clsDataAccess.Execute("spTeamShipAnswersUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iTeamShipAnswerID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iTeamShipAnswerID", iTeamShipAnswerID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spTeamShipAnswersDelete", sqlParameter);
        }

        public static DataTable GetTeamShipAnswersList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spTeamShipAnswersList", EmptySqlParameter);
        }
        public static DataTable GetTeamShipAnswersList(string strFilterExpression, string strSortExpression)
        {
            DataView dvTeamShipAnswersList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvTeamShipAnswersList = clsDataAccess.GetDataView("spTeamShipAnswersList", EmptySqlParameter);
            dvTeamShipAnswersList.RowFilter = strFilterExpression;
            dvTeamShipAnswersList.Sort = strSortExpression;

            return dvTeamShipAnswersList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iTeamShipAnswerID", m_iTeamShipAnswerID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spTeamShipAnswersGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iTeamShipID = Convert.ToInt32(drRecord["iTeamShipID"]);
                m_iMissionID = Convert.ToInt32(drRecord["iMissionID"]);
                m_strNameAnswer = drRecord["strNameAnswer"].ToString();
                m_strTitleAnswer = drRecord["strTitleAnswer"].ToString();
                m_bIsDisplayed = Convert.ToBoolean(drRecord["bIsDisplayed"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}