
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCustomLoginColours
/// </summary>
public class clsCustomLoginColours
{
    #region MEMBER VARIABLES

     private     int m_iCustomLoginColourID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
    private string m_strTitle;
     private     string m_strPathToImages;
     private     string m_strMasterImage;
    private string m_strLink;
     private     string m_strPanelBackgroundColour;
     private     string m_strRememberMeTextColour;
     private     string m_strButtonBackgroundColour;
     private     string m_strButtonTextColour;
     private     string m_strTextBelowButtonColour;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iCustomLoginColourID
    {
        get
        {
            return m_iCustomLoginColourID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }
    

        public string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    string strPathToImages
    {
        get
        {
            return m_strPathToImages;
        }
        set
        {
            m_strPathToImages = value;
        }
    }

    public    string strMasterImage
    {
        get
        {
            return m_strMasterImage;
        }
        set
        {
            m_strMasterImage = value;
        }
    }

    public string strLink
    {
        get
        {
            return m_strLink;
        }
        set
        {
            m_strLink = value;
        }
    }

    public    string strPanelBackgroundColour
    {
        get
        {
            return m_strPanelBackgroundColour;
        }
        set
        {
            m_strPanelBackgroundColour = value;
        }
    }

    public    string strRememberMeTextColour
    {
        get
        {
            return m_strRememberMeTextColour;
        }
        set
        {
            m_strRememberMeTextColour = value;
        }
    }

    public    string strButtonBackgroundColour
    {
        get
        {
            return m_strButtonBackgroundColour;
        }
        set
        {
            m_strButtonBackgroundColour = value;
        }
    }

    public    string strButtonTextColour
    {
        get
        {
            return m_strButtonTextColour;
        }
        set
        {
            m_strButtonTextColour = value;
        }
    }

    public    string strTextBelowButtonColour
    {
        get
        {
            return m_strTextBelowButtonColour;
        }
        set
        {
            m_strTextBelowButtonColour = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsCustomLoginColours()
    {
        m_iCustomLoginColourID = 0;
    }

    public clsCustomLoginColours(int iCustomLoginColourID)
    {
        m_iCustomLoginColourID = iCustomLoginColourID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iCustomLoginColourID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strPathToImages", m_strPathToImages),
                        new SqlParameter("@strMasterImage", m_strMasterImage),
                        new SqlParameter("@strLink", m_strLink),
                        new SqlParameter("@strPanelBackgroundColour", m_strPanelBackgroundColour),
                        new SqlParameter("@strRememberMeTextColour", m_strRememberMeTextColour),
                        new SqlParameter("@strButtonBackgroundColour", m_strButtonBackgroundColour),
                        new SqlParameter("@strButtonTextColour", m_strButtonTextColour),
                        new SqlParameter("@strTextBelowButtonColour", m_strTextBelowButtonColour)                  
                  };

                  //### Add
                  m_iCustomLoginColourID = (int)clsDataAccess.ExecuteScalar("spCustomLoginColoursInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iCustomLoginColourID", m_iCustomLoginColourID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strPathToImages", m_strPathToImages),
                         new SqlParameter("@strMasterImage", m_strMasterImage),
                         new SqlParameter("@strLink", m_strLink),
                         new SqlParameter("@strPanelBackgroundColour", m_strPanelBackgroundColour),
                         new SqlParameter("@strRememberMeTextColour", m_strRememberMeTextColour),
                         new SqlParameter("@strButtonBackgroundColour", m_strButtonBackgroundColour),
                         new SqlParameter("@strButtonTextColour", m_strButtonTextColour),
                         new SqlParameter("@strTextBelowButtonColour", m_strTextBelowButtonColour)
          };
          //### Update
          clsDataAccess.Execute("spCustomLoginColoursUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iCustomLoginColourID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iCustomLoginColourID", iCustomLoginColourID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spCustomLoginColoursDelete", sqlParameter);
        }

        public static DataTable GetCustomLoginColoursList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spCustomLoginColoursList", EmptySqlParameter);
        }
        public static DataTable GetCustomLoginColoursList(string strFilterExpression, string strSortExpression)
        {
            DataView dvCustomLoginColoursList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvCustomLoginColoursList = clsDataAccess.GetDataView("spCustomLoginColoursList", EmptySqlParameter);
            dvCustomLoginColoursList.RowFilter = strFilterExpression;
            dvCustomLoginColoursList.Sort = strSortExpression;

            return dvCustomLoginColoursList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iCustomLoginColourID", m_iCustomLoginColourID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spCustomLoginColoursGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);
            m_strTitle = drRecord["strTitle"].ToString();
            m_strPathToImages = drRecord["strPathToImages"].ToString();
                   m_strMasterImage = drRecord["strMasterImage"].ToString();
            m_strLink = drRecord["strLink"].ToString();
            m_strPanelBackgroundColour = drRecord["strPanelBackgroundColour"].ToString(); 
                    m_strRememberMeTextColour = drRecord["strRememberMeTextColour"].ToString();
                   m_strButtonBackgroundColour = drRecord["strButtonBackgroundColour"].ToString();
                   m_strButtonTextColour = drRecord["strButtonTextColour"].ToString();
                   m_strTextBelowButtonColour = drRecord["strTextBelowButtonColour"].ToString();
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}