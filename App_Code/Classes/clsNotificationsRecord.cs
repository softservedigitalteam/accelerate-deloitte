
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsNotifications
/// </summary>
public class clsNotifications
{
    #region MEMBER VARIABLES

        private int m_iNotificationID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iAccountUserID;
    private int m_iGroupID;
        private int m_iMissionID;
        private String m_strMessage;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iNotificationID
        {
            get
            {
                return m_iNotificationID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iAccountUserID
        {
            get
            {
                return m_iAccountUserID;
            }
            set
            {
                m_iAccountUserID = value;
            }
        }

    public int iGroupID
    {
        get
        {
            return m_iGroupID;
        }
        set
        {
            m_iGroupID = value;
        }
    }

    public int iMissionID
        {
            get
            {
                return m_iMissionID;
            }
            set
            {
                m_iMissionID = value;
            }
        }

        public String strMessage
        {
            get
            {
                return m_strMessage;
            }
            set
            {
                m_strMessage = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsNotifications()
    {
        m_iNotificationID = 0;
    }

    public clsNotifications(int iNotificationID)
    {
        m_iNotificationID = iNotificationID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iNotificationID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iAccountUserID", m_iAccountUserID),
                        new SqlParameter("@iGroupID", m_iGroupID),
                        new SqlParameter("@iMissionID", m_iMissionID),
                        new SqlParameter("@strMessage", m_strMessage)                  
                  };

                  //### Add
                  m_iNotificationID = (int)clsDataAccess.ExecuteScalar("spNotificationsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iNotificationID", m_iNotificationID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iAccountUserID", m_iAccountUserID),
                         new SqlParameter("@iGroupID", m_iGroupID),
                         new SqlParameter("@iMissionID", m_iMissionID),
                         new SqlParameter("@strMessage", m_strMessage)
                    };
                    //### Update
                    clsDataAccess.Execute("spNotificationsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iNotificationID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iNotificationID", iNotificationID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spNotificationsDelete", sqlParameter);
        }

        public static DataTable GetNotificationsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spNotificationsList", EmptySqlParameter);
        }

    public static DataTable GetGroupNotificationsList(int iGroupID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iGroupID", iGroupID)
        };
        return clsDataAccess.GetDataTable("spNotificationsGroupList", sqlParameter);
    }
    

        public static DataTable GetNotificationsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvNotificationsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvNotificationsList = clsDataAccess.GetDataView("spNotificationsList", EmptySqlParameter);
            dvNotificationsList.RowFilter = strFilterExpression;
            dvNotificationsList.Sort = strSortExpression;

            return dvNotificationsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iNotificationID", m_iNotificationID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spNotificationsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iAccountUserID = Convert.ToInt32(drRecord["iAccountUserID"]);
            m_iGroupID = Convert.ToInt32(drRecord["iGroupID"]);
            m_iMissionID = Convert.ToInt32(drRecord["iMissionID"]);
                m_strMessage = drRecord["strMessage"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}