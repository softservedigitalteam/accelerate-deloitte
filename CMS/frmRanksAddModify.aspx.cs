using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmRanksAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsRanks clsRanks;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iRankID is passed through then we want to instantiate the object with that iRankID
            if ((Request.QueryString["iRankID"] != "") && (Request.QueryString["iRankID"] != null))
            {
                clsRanks = new clsRanks(Convert.ToInt32(Request.QueryString["iRankID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsRanks = new clsRanks();
            }
            Session["clsRanks"] = clsRanks;
        }
        else
        {
            clsRanks = (clsRanks)Session["clsRanks"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmRanksView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtPointLevel, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Rank added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Rank not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtPointLevel.Text = "";
        clsValidation.SetValid(txtPointLevel);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsRanks.strTitle;
         txtPointLevel.Text = clsRanks.iPointLevel.ToString();
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsRanks.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsRanks.iAddedBy = clsUsers.iUserID;
        clsRanks.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsRanks.iEditedBy = clsUsers.iUserID;
        clsRanks.strTitle = txtTitle.Text;
        clsRanks.iPointLevel = Convert.ToInt32(txtPointLevel.Text);

        clsRanks.Update();

        Session["dtRanksList"] = null;

        //### Go back to view page
        Response.Redirect("frmRanksView.aspx");
    }

    #endregion
}