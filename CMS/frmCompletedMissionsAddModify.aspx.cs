using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmCompletedMissionsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsCompletedMissions clsCompletedMissions;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popAccountUser();
             popMission();

            //### If the iCompletedMissionID is passed through then we want to instantiate the object with that iCompletedMissionID
            if ((Request.QueryString["iCompletedMissionID"] != "") && (Request.QueryString["iCompletedMissionID"] != null))
            {
                clsCompletedMissions = new clsCompletedMissions(Convert.ToInt32(Request.QueryString["iCompletedMissionID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsCompletedMissions = new clsCompletedMissions();
            }
            Session["clsCompletedMissions"] = clsCompletedMissions;
        }
        else
        {
            clsCompletedMissions = (clsCompletedMissions)Session["clsCompletedMissions"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmCompletedMissionsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(lstAccountUser, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstMission, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">CompletedMission added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - CompletedMission not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        lstAccountUser.SelectedValue = "0";
        clsValidation.SetValid(lstAccountUser);
        lstMission.SelectedValue = "0";
        clsValidation.SetValid(lstMission);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         lstAccountUser.SelectedValue = clsCompletedMissions.iAccountUserID.ToString();
         lstMission.SelectedValue = clsCompletedMissions.iMissionID.ToString();
    }
    
    private void popAccountUser()
    {
         DataTable dtAccountUsersList = new DataTable();
         lstAccountUser.DataSource = clsAccountUsers.GetAccountUsersList();

         //### Populates the drop down list with PK and TITLE;
         lstAccountUser.DataValueField = "iAccountUserID";
         lstAccountUser.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstAccountUser.DataBind();

         //### Add default select option;
         lstAccountUser.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    private void popMission()
    {
         DataTable dtMissionsList = new DataTable();
         lstMission.DataSource = clsMissions.GetMissionsList();

         //### Populates the drop down list with PK and TITLE;
         lstMission.DataValueField = "iMissionID";
         lstMission.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstMission.DataBind();

         //### Add default select option;
         lstMission.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsCompletedMissions.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCompletedMissions.iAddedBy = clsUsers.iUserID;
        clsCompletedMissions.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCompletedMissions.iEditedBy = clsUsers.iUserID;
        clsCompletedMissions.iAccountUserID = Convert.ToInt32(lstAccountUser.SelectedValue.ToString());
        clsCompletedMissions.iMissionID = Convert.ToInt32(lstMission.SelectedValue.ToString());

        clsCompletedMissions.Update();

        Session["dtCompletedMissionsList"] = null;

        //### Go back to view page
        Response.Redirect("frmCompletedMissionsView.aspx");
    }

    #endregion
}