﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" CodeFile="frmLandingZone.aspx.cs" Inherits="CMS_frmLandingZone" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!--Dashboard content-->
    <div class="dashboardContent">
        <asp:Literal ID="litReport" runat="server"></asp:Literal>
    </div>

</asp:Content>

