using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmMissionAnswersAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsMissionAnswers clsMissionAnswers;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popMissionQuestion();

            //### If the iMissionAnswerID is passed through then we want to instantiate the object with that iMissionAnswerID
            if ((Request.QueryString["iMissionAnswerID"] != "") && (Request.QueryString["iMissionAnswerID"] != null))
            {
                clsMissionAnswers = new clsMissionAnswers(Convert.ToInt32(Request.QueryString["iMissionAnswerID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsMissionAnswers = new clsMissionAnswers();
            }
            Session["clsMissionAnswers"] = clsMissionAnswers;
        }
        else
        {
            clsMissionAnswers = (clsMissionAnswers)Session["clsMissionAnswers"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmMissionAnswersView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(lstMissionQuestion, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtAnswer, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">MissionAnswer added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - MissionAnswer not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        lstMissionQuestion.SelectedValue = "0";
        clsValidation.SetValid(lstMissionQuestion);
        txtAnswer.Text = "";
        clsValidation.SetValid(txtAnswer);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        lstMissionQuestion.SelectedValue = clsMissionAnswers.iMissionQuestionID.ToString();
        txtAnswer.Text = clsMissionAnswers.strAnswer;

        if (clsMissionAnswers.bIsCorrectAnswer)
            cbIsCorrect.Checked = true;
    }
    
    private void popMissionQuestion()
    {
         DataTable dtMissionQuestionsList = new DataTable();
         lstMissionQuestion.DataSource = clsMissionQuestions.GetMissionQuestionsList();

         //### Populates the drop down list with PK and TITLE;
         lstMissionQuestion.DataValueField = "iMissionQuestionID";
         lstMissionQuestion.DataTextField = "strQuestion";

         //### Bind the data to the list;
         lstMissionQuestion.DataBind();

         //### Add default select option;
         lstMissionQuestion.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsMissionAnswers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissionAnswers.iAddedBy = clsUsers.iUserID;
        clsMissionAnswers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissionAnswers.iEditedBy = clsUsers.iUserID;
        clsMissionAnswers.iMissionQuestionID = Convert.ToInt32(lstMissionQuestion.SelectedValue.ToString());
        clsMissionAnswers.strAnswer = txtAnswer.Text;

        clsMissionAnswers.bIsCorrectAnswer = cbIsCorrect.Checked;

        clsMissionAnswers.Update();

        Session["dtMissionAnswersList"] = null;

        //### Go back to view page
        Response.Redirect("frmMissionAnswersView.aspx");
    }

    #endregion
}