using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmAdminEmailSettingsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsAdminEmailSettings clsAdminEmailSettings;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             //popAdminEmailSettings();

            //### If the iAdminEmailSettingID is passed through then we want to instantiate the object with that iAdminEmailSettingID
            if ((Request.QueryString["iAdminEmailSettingID"] != "") && (Request.QueryString["iAdminEmailSettingID"] != null))
            {
                clsAdminEmailSettings = new clsAdminEmailSettings(Convert.ToInt32(Request.QueryString["iAdminEmailSettingID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsAdminEmailSettings = new clsAdminEmailSettings();
            }
            Session["clsAdminEmailSettings"] = clsAdminEmailSettings;
        }
        else
        {
            clsAdminEmailSettings = (clsAdminEmailSettings)Session["clsAdminEmailSettings"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmAdminEmailSettingsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;
        bCanSave = clsValidation.IsNullOrEmpty(txtAdminTitle, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtEmail1, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">AdminEmailSetting added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - AdminEmailSetting not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        //lstAdminEmailSettings.SelectedValue = "0";
        //clsValidation.SetValid(lstAdminEmailSettings);
        txtAdminTitle.Text = "";
        clsValidation.SetValid(txtAdminTitle);
        txtEmail1.Text = "";
        clsValidation.SetValid(txtEmail1);
        txtEmail2.Text = "";
        clsValidation.SetValid(txtEmail2);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         //lstAdminEmailSettings.SelectedValue = clsAdminEmailSettings.iAdminEmailSettingsID.ToString();
         txtAdminTitle.Text = clsAdminEmailSettings.strAdminTitle;
         txtEmail1.Text = clsAdminEmailSettings.strEmail1;
         txtEmail2.Text = clsAdminEmailSettings.strEmail2;
    }
    
    //private void popAdminEmailSettings()
    //{
    //     DataTable dtAdminEmailSettingssList = new DataTable();
    //     lstAdminEmailSettings.DataSource = clsAdminEmailSettings.GetAdminEmailSettingsList();

    //     //### Populates the drop down list with PK and TITLE;
    //     lstAdminEmailSettings.DataValueField = "iAdminEmailSettingsID";
    //     lstAdminEmailSettings.DataTextField = "strAdminTitle";

    //     //### Bind the data to the list;
    //     lstAdminEmailSettings.DataBind();

    //     //### Add default select option;
    //     lstAdminEmailSettings.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    //}
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        //clsAdminEmailSettings.iAdminEmailSettingsID = Convert.ToInt32(lstAdminEmailSettings.SelectedValue.ToString());
        clsAdminEmailSettings.strAdminTitle = txtAdminTitle.Text;
        clsAdminEmailSettings.strEmail1 = txtEmail1.Text;
        clsAdminEmailSettings.strEmail2 = txtEmail2.Text;

        clsAdminEmailSettings.Update();

        Session["dtAdminEmailSettingsList"] = null;

        //### Go back to view page
        Response.Redirect("frmAdminEmailSettingsView.aspx");
    }

    #endregion
}